﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvisiButton : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public Text textDisplay;
    public float speed = 1f;

    bool fadeIn;

    private void Start()
    {
        fadeIn = false;
        canvasGroup.alpha = 0f;
    }

    private void Update()
    {
        if (fadeIn)
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1f, Time.deltaTime * speed);
        }
        else
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0f, Time.deltaTime * speed);
        }
    }
    public void Enter(string toDisplay)
    {
        print("ENTER~");
        fadeIn = true;
        textDisplay.text = toDisplay;
    }

    public void Exit ()
    {
        print("EXIT!");
        fadeIn = false;
    }


}
